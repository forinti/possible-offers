<?php

use Faker\Generator as Faker;

$factory->define(App\Offer::class, function (Faker $faker) {
    return [
        'title'=>$faker->sentence('3'),
        'country_id'=>$faker->numberBetween(1,3),
        'price'=>$faker->numberBetween(1000,3500),
        'meters'=>$faker->numberBetween(20,100),
    ];
});


