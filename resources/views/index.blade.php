@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Page #{{request()->get('page') ?   request()->get('page') : '1'}}</li>
        </ol>
    </nav>
    @foreach($groups as $group)
        <h4>Group #{{$group->id}}</h4>
        <ul class="list-group">
            @foreach($group->offers as $offer)
                <li class="list-group-item @if($offer->isBase($group->id)) bg-primary @endif @if($offer->isCopy($group->id)) bg-secondary @endif">{{$offer->title}} // Price: {{$offer->price}} // Meters: {{$offer->meters}}
                  @if($offer->isBase($group->id))
                        <a href="{{route('removeBaseStatus',['id'=>$group->id, 'offerId'=>$offer->id,'page'=>request()->get('page')])}}" class = "btn btn-danger" aria-haspopup="true" >Remove base status</a>
                    @elseif($offer->isCopy($group->id))
                        <a href="{{route('removeCopyStatus',['id'=>$group->id, 'offerId'=>$offer->id,'page'=>request()->get('page')])}}" class ="btn btn-warning"> Remove copy status</a>
                    @elseif($group->hasBase() && !$offer->isBase($group->id))
                        <a href="{{route('setCopyStatus',['id'=>$group->id, 'offerId'=>$offer->id,'page'=>request()->get('page')])}}" class = "btn btn-secondary"> Assign as copy</a>
                      @else
                        <a href="{{route('setBaseStatus',['id'=>$group->id, 'offerId'=>$offer->id,'page'=>request()->get('page')])}}" class = "btn btn-primary">Assign as base</a>
                    @endif
                </li>
            @endforeach
        </ul>
    @endforeach
    <div class="row" style = "margin-top:10px">
        <div class="col-md-2 offset-4">{{$groups->links()}}</div>
    </div>

@endsection