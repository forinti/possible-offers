<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','IndexController@index');
Route::get('/test','IndexController@test');
Route::get('/groups/{id}/setbasestatus/{offerId}','OffersController@setBaseStatus')->name('setBaseStatus');
Route::get('/groups/{id}/removebasestatus/{offerId}','OffersController@removeBaseStatus')->name('removeBaseStatus');
Route::get('/groups/{id}/setcopystatus/{offer}','OffersController@setCopyStatus')->name('setCopyStatus');
Route::get('/groups/{id}/removecopystatus/{offer}','OffersController@removeCopyStatus')->name('removeCopyStatus');

