<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Offer;
use App\Group;
use Illuminate\Support\Facades\DB;

class FindAndGroupPossiblySameOffers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'offers:group';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Finds all possibly same offers and groups them';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle()
    {

        echo "\n Start of deleting previous groups \n";

        foreach (Group::get() as $group)
            $group->delete();

        echo "\n End of deleting \n";
        $o = Offer::orderByRaw('country_id, price, meters DESC')->get();
        $offers = $o->toArray();
        $groups = [];
        $groupCounter = 0;
        $currentOffer = null;
        $lastKey = key(array_slice($offers, -1, 1, TRUE));
        echo "\n Start of group\n";
        while ($offers !== []) {
            foreach ($offers as $key => $offer) {
                $countryFlag = false;
                $priceFlag = false;
                $metersFlag = false;

                if (!$currentOffer) {
                    $currentOffer = $offer;
                    $groups[$groupCounter][] = $offer['id'];
                    unset($offers[$key]);
                } else {
                    if ($currentOffer['country_id'] === $offer['country_id']) {
                        $countryFlag = true;
                    }

                    if (abs($currentOffer['price'] - $offer['price']) < ($currentOffer['price'] + $offer['price']) * 0.05) {
                        $priceFlag = true;
                    }

                    if (abs($currentOffer['meters'] - $offer['meters']) < ($currentOffer['meters'] + $offer['meters']) * 0.02) {
                        $metersFlag = true;
                    }

                    if ($countryFlag && $priceFlag && $metersFlag) {
                        $groups[$groupCounter][] = $offer['id'];
                        unset($offers[$key]);

                        if ($key === $lastKey) {
                            $lastKey = key(array_slice($offers, -1, 1, TRUE));
                        }
                    }

                    if (!$countryFlag && !$priceFlag && !$metersFlag) {
                        $groupCounter++;
                        $currentOffer = null;
                        break;
                    }

                    if ($lastKey === $key) {
                        $groupCounter++;
                        $currentOffer = null;
                    }
                }
            }
        }

        echo "\n End of Group \n";

        echo "\n Start saving groups in DB\n";

        foreach ($groups as $offers) {
            if (count($offers) > 1) {
                $gr = new Group();
                $gr->save();
                $gr->offers()->attach($offers);
            }
        }
        echo "\n End of saving groups \n";
        echo "\n Start of detaching not same offers from groups \n";
        $groups = Group::get();

        foreach ($groups as $group) {
            $group->detachNotSame();
        }

        echo "\n End of detaching \n";

        echo "\n Start of deleting groups with one offer \n";
        foreach ($groups as $group) {
            if ($group->offers->count() < 2)
                $group->delete();
        }
        echo "\n End of deleting \n";
        echo "\n Success \n";
    }
}
