<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Group extends Model
{
    public function offers()
    {
        return $this->belongsToMany('App\Offer', 'group_offer', 'group_id', 'offer_id');
    }

    public function hasBase()  {
       $pivots = DB::table('group_offer')->where('group_id',$this->id)->get();
       foreach($pivots as $pivot) {
           if ($pivot->is_base === 1)
               return true;
       }
       return false;
    }
    public function check()
    {
        $offers = $this->offers;
        foreach ($offers as $offer) {
            foreach ($offers as $o) {
                if (!$offer->isSame($o))
                    return false;
            }
        }
        return true;
    }

    public function detachNotSame()
    {
        $offers = $this->offers;
        $table = [];
        foreach ($offers as $offer) {
            $table [$offer->id] = 0;
        }
        foreach ($offers as $offer) {
            foreach ($offers as $o) {
                if ($offer->isSame($o))
                    $table[$offer->id]++;
            }
        }
        asort($table);
        $keys = array_keys($table);
        if (!$this->check()) {
            $this->offers()->detach($keys[0]);
            $this->refresh();
            $this->fresh('offers');

            if (!$this->check())
                $this->detachNotSame();
        }
    }
}
