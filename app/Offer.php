<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Offer extends Model
{

    public function group()
    {
        return $this->belongsToMany('App\Group', 'group_offer');
    }

    public function isBase($groupId)
    {
        $pivot = DB::table('group_offer')->where('group_id', $groupId)->where('offer_id', $this->id)->where('is_base',1)->first();
        return !is_null($pivot);
    }

    public function isCopy($groupId)
    {
        $pivot = DB::table('group_offer')->where('group_id', $groupId)->where('offer_id', $this->id)->where('is_base',0)->first();
        return !is_null($pivot);
    }

    public function isSame(Offer $offer)
    {
        return ((abs($this->price - $offer->price) < ($this->price + $offer->price) * 0.05)
            && (abs($this->meters - $offer->meters) < ($this->meters + $offer->meters) * 0.02));
    }
}
