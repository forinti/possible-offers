<?php

namespace App\Http\Controllers;

use App\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OffersController extends Controller
{
    public function setBaseStatus($groupId, $offerId) {
        $pivots = DB::table('group_offer')->where('group_id',$groupId)->get();
        $page = request()->get('page');
        $redirectTo = $page? "/?page=$page" :'/';
        foreach($pivots as $pivot) {
            if ($pivot->is_base === 1)
                return redirect($redirectTo);
        }
        DB::table('group_offer')->where('group_id',$groupId)->where('offer_id',$offerId)->update(['is_base'=>1]);
        return redirect($redirectTo);

    }
    public function removeBaseStatus($groupId, $offerId) {
        $pivot = DB::table('group_offer')->where('group_id',$groupId)->where('offer_id',$offerId)->first();
        $page = request()->get('page');
        $redirectTo = $page? "/?page=$page" :'/';
        if ($pivot->is_base === 1)
            DB::table('group_offer')->where('group_id',$groupId)->update(['is_base'=>null]);

        return redirect($redirectTo);
    }
    public function setCopyStatus($groupId, $offerId) {
        $page = request()->get('page');
        $redirectTo = $page? "/?page=$page" :'/';

        $group = Group::find($groupId);
        if ($group->hasBase())
            DB::table('group_offer')->where('group_id',$groupId)->where('offer_id',$offerId)->update(['is_base'=>0]);

       return redirect($redirectTo);
    }
    public function removeCopyStatus($groupId, $offerId) {
        $page = request()->get('page');
        $redirectTo = $page? "/?page=$page" :'/';

        $pivot = DB::table('group_offer')->where('group_id',$groupId)->where('offer_id',$offerId)->first();

        if ($pivot->is_base === 0)
            DB::table('group_offer')->where('group_id',$groupId)->where('offer_id',$offerId)->update(['is_base'=>null]);

        return redirect($redirectTo);
    }
}
