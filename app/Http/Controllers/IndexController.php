<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Offer;
use App\Group;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{

    public function index()
    {
        $groups = Group::with('offers')->paginate(10);
        return view('index', ['groups' => $groups]);
    }

    public function test()
    {
         $y = 0;
         $n = 0;
         $groups = Group::with('offers')->get();
         var_dump($groups->count());
         echo "<hr>";

         foreach($groups as $group) {
             if ($group->check())
                 $y++;
             else
                 $n++;

             var_dump($group->check());
             var_dump($group->offers->count());
             echo("#$group->id");
             echo "<br>";
         }
         dd($y, $n);
    }
}
