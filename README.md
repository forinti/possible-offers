##Данный проект группирует предложения о продаже квартир по группам, в которых каждое предложение похоже с каждым другим из этой группы.  

 ####Два предложения являются похожими если сразу выполняются следующие условия:
   
 ```
 1. country_id совпадают
 2. разница между их стоимостями (price) меньше следующего условия:
 < (offer1.price+offer2.price)*0.05
 3. разница между их кв. метрами (metres) меньше следующего условия:
 < (offer1.metres+offer2.metres)*0.02
 ```

 Как развернуть проект: 
 1. cd /path/to/project/
 2. composer install
 3. php artisan key:generate
 4. php artisan migrate
 5. php artisan db:seed (если нужны данные для теста)
 6. php artisan offers:group - запуск группировки данных
 7. php artisan serve

 
Полученные группы можно посмотреть по пути '/'

ngixn config: 
```
server {
   listen 80;
   listen [::]:80;

   root /path/to/project/public;
   index index.php index.html index.htm;

   server_name possible_offers;

   location / {
       try_files $uri $uri/ /index.php$is_args$args;
   }

   location ~ \.php$ {
       try_files $uri /index.php =404;

       fastcgi_split_path_info ^(.+\.php)(/.+)$;
       fastcgi_pass unix:/run/php/php7.2-fpm.sock;
       fastcgi_index index.php;
       fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
       include /etc/nginx/fastcgi_params;
   }
}
```
 
 